import React, { Component } from 'react'
import { connect } from 'react-redux'
import ArticlesList from './ArticlesList'
import { itemsFetchData } from '../actions/items';
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card';
class Home extends Component {
  componentDidMount(){
    this.props.fetchData(`https://content.guardianapis.com/search?api-key=fc7fe84a-f8b5-4f57-a198-9ee0ac5ce766&page-size=12`, `&page=1`)
  }

  handleClick = (e) => {
    let history = localStorage.getItem('@great-news/history')
    history = history ? history.split(',') : [ ]
    const articleId = e.currentTarget.dataset.id
    history.push(articleId)
    localStorage.setItem('@great-news/history', history.toString())
  }

  render() {
    const newsHistory = (localStorage.getItem('@great-news/history') !== null) ? (localStorage.getItem('@great-news/history')
      .split(',')
      .map((el, index) => {
      return (
        <li key={index} className='history__item'>{el}</li>
      )
    })) :
    (null)
    const visited = (localStorage.getItem('@great-news/history') !== null) ? (
      <Card className='history'>
        <h4>Artigos visitados</h4>
        <ol className='history__list'>
          {newsHistory}
        </ol>
      </Card>
      ) : (
        null
      )

    if (this.props.hasErrored) {
      return <p>Sorry! There was an error loading the items</p>;
    }
    if (this.props.isLoading) {
        return <div className='loading'></div>;
    }
    const {items} = this.props
    const articles = items ? items.results : null
    return (
      <div className='home'>
        <Container>
          <h1 className='home__title'>The Protector</h1>
          <ArticlesList items={articles} handleClick = {this.handleClick}/> 
          { visited }         
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
      items: state.items,
      hasErrored: state.itemsHasErrored,
      isLoading: state.itemsIsLoading
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      fetchData: (url, page) => dispatch(itemsFetchData(url, page))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home)