import React from 'react' 
import moment from 'moment'
import { Link } from 'react-router-dom'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const ArticlesList = ({items, handleClick}) => {  
  const articles = items ? items.map(
    item => (
      <Card className="news-card" key={item.id}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            {item.sectionName}
          </Typography>
          <Typography variant="h5" component="h2">
            {item.webTitle}
          </Typography>
          <Typography color="textSecondary">
            {moment(item.webPublicationDate).format('DD/MM/YYYY, H:mm:ss')}
          </Typography>             
        </CardContent>
        <CardActions>
          <Link to={'/' + item.id} onClick={handleClick} data-id={item.id}>
            <Button size="small" className="news-card__btn">Saiba mais</Button>              
          </Link>
        </CardActions>
      </Card>
    )
    ) : (
      <div className='loading'></div>
    )
  return (
    <div className='news-cards__wrapper'>
      {articles}
    </div>
     
  )
}

export default ArticlesList