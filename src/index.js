import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { Provider } from 'react-redux'
// import { createStore, applyMiddleware } from 'redux'
// import promise from 'redux-promise'
// import reducers from './reducers'

import configureStore from './store/configureStore';
const store = configureStore();
// const store = applyMiddleware(promise)(createStore)


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , document.getElementById('root'));


