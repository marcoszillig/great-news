import { combineReducers } from 'redux';
import { items, itemsHasErrored, itemsIsLoading, item } from './items';
export default combineReducers({
    items,
    itemsHasErrored,
    itemsIsLoading,
    item
});