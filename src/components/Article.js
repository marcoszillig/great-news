import React, { Component } from 'react'
// import axios from 'axios'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Parser from 'html-react-parser';
import { itemFetchData } from '../actions/items';


class Article extends Component {
  componentDidMount(){
    let pathname = this.props.location.pathname
    const ROOT_URL = 'https://content.guardianapis.com'
    const API_KEY = '?api-key=fc7fe84a-f8b5-4f57-a198-9ee0ac5ce766'
    const PARAMS = '&show-fields=body'
    this.props.fetchItem(`${ROOT_URL}${pathname}${API_KEY}${PARAMS}`)
  }
  render() {
    const {item} = this.props
    const body = item.fields ? Parser(item.fields.body) : null

    if (this.props.hasErrored) {
      return <p>Sorry! There was an error loading the items</p>;
    }
    if (this.props.isLoading) {
        return <div className='loading'></div>;
    }
    return (
      <div className="article">
        <Link to='/' className="article__back-btn">
          &#8678;
        </Link>        
        <div className='article__content'>
          <h2 className='article__title'>{item.webTitle}</h2>
          { body }
        </div> 
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    item: state.item,
    hasErrored: state.itemsHasErrored,
    isLoading: state.itemsIsLoading
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
      fetchItem: (url) => dispatch(itemFetchData(url))
  };
};
// class Article extends Component {
//   state = {
//     article: null,
//     pathname: null
//   }
  
//   componentDidMount(){
//     let pathname = this.props.location.pathname
//     const ROOT_URL = 'https://content.guardianapis.com'
//     const API_KEY = '?api-key=fc7fe84a-f8b5-4f57-a198-9ee0ac5ce766'
//     const PARAMS = '&show-fields=body'
    
//     axios.get(`${ROOT_URL}${pathname}${API_KEY}${PARAMS}` )
//       .then(res => {
//         this.setState({
//           article: res.data.response.content
//         })
//     })    
//   }

  

//   render() {
//     const  article  = this.state.article ? (
//       <div>
        // <h4>{this.state.article.webTitle}</h4>
        // {Parser(this.state.article.fields.body)}
//       </div>
//       ) : (
//         <div >
//           Loading postMessage...
//         </div>
//       )
//     return (
//       <div>
//         <div className="article__title-bar">
//           <Link to='/' className="article__back-btn">
//               &#8678;
//           </Link>
//           <h1>The Protector</h1>
//         </div>
//         <Container maxWidth='md' className="article__wrapper">          
//             {article}
//         </Container>
//       </div>
//     )
//   }
// }

export default connect(mapStateToProps, mapDispatchToProps)(Article)