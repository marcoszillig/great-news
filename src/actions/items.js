import axios from 'axios'

export function itemsHasErrored(bool) {
  return {
    type: 'ITEMS_HAS_ERRORED',
    hasErrored: bool
  };
}
export function itemsIsLoading(bool) {
  return {
    type: 'ITEMS_IS_LOADING',
    isLoading: bool
  };
}
export function itemsFetchDataSuccess(items, page) {
  return {
    type: 'ITEMS_FETCH_DATA_SUCCESS',
    items,
    page
  };
}
export function itemFetchDataSuccess(item) {
  return {
    type: 'ITEM_FETCH_DATA_SUCCESS',
    item
  };
}

export function errorAfterFiveSeconds() {
  return (dispatch) => {
      setTimeout(() => {
          dispatch(itemsHasErrored(true));
      }, 5000);
  };
}

export function itemsFetchData(url,page) {
  return (dispatch) => {
    dispatch(itemsIsLoading(true));
    axios.get(url + page)
      .then((response) => {
        if (response.status !== 200) {
          throw Error(response.data.statusText);
        }
        
        dispatch(itemsIsLoading(false));
        return response;
      })
      .then((items) => dispatch(itemsFetchDataSuccess(items.data.response)))
      .catch(() => dispatch(itemsHasErrored(true)));
  };
}

export function itemFetchData(url) {
  return (dispatch) => {
      dispatch(itemsIsLoading(true));
      axios.get(url)
          .then((response) => {
              if (response.status !== 200) {
                  throw Error(response.data.statusText);
              }
              
              dispatch(itemsIsLoading(false));
              return response;
          })
          .then((item) => dispatch(itemFetchDataSuccess(item.data.response.content)))
          .catch(() => dispatch(itemsHasErrored(true)));
  };
}
